<?php
// +----------------------------------------------------------------------
// | SparkShop 坚持做优秀的商城系统
// +----------------------------------------------------------------------
// | Copyright (c) 2022~2099 http://sparkshop.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai  <876337011@qq.com>
// +----------------------------------------------------------------------
namespace app\api\controller;

use app\api\service\IndexService;
use app\BaseController;
use app\service\CartService;
use app\service\OrderService;

class Index extends BaseController
{
    public function initialize()
    {
        crossDomain();
    }

    /**
     * 小程序首页
     */
    public function index()
    {
        $indexService = new IndexService();
        $data = $indexService->getIndexData();
        return json($data);
    }

    /**
     * 搜索
     */
    public function search()
    {
        $indexService = new IndexService();
        $data = $indexService->search(input('post.'));
        return json($data);
    }

    /**
     * 获取营销插件的配置
     */
    public function marketingConfig()
    {
        $orderService = new OrderService();
        return json($orderService->getMarketingConfig());
    }

    /**
     * 获取购物车数量
     */
    public function cartNum()
    {
        $cartService = new CartService();
        $cartInfo = $cartService->getCartNum();
        return json($cartInfo);
    }
}